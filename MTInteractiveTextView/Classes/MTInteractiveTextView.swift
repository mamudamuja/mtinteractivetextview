//
//  MTInteractiveTextView.swift
//  MTInteractiveTextView
//
//  Created by Milutin Tomic on 27.11.17.
//

import UIKit

/// Instances of this class are used to hold a reference
/// to an action as well as it's range in the text
private class MTInteractiveTextViewAction: NSObject {
    var action: (() -> Void)? = {}
    var actionRange: NSRange?
    var attributes: [String: Any]?
}

/// This UITextView subclass lets the user define
/// actions fot certain parts of the text it is displaying.
/// It is not meant to be used as a text editing area.
/// To ensure interaction the scrolling and editing
/// features must remain disabled.
public class MTInteractiveTextView: UITextView {

    /// Stores the added actions
    fileprivate var actions: [MTInteractiveTextViewAction] = []
    
    // MARK: - Overrides
    
    public override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        guard let touch = touches.first else { return }
        
        // get touch point
        let touchPoint = touch.location(in: self)
        
        for action in actions {
            guard let range = action.actionRange else { continue }
            let boundingBox = self.boundingBoxForCharacters(inRange: range)
            
            // execute action if touch was in area
            if boundingBox.contains(touchPoint) {
                action.action?()
                return
            }
        }
    }
    
    /// TextView should not be editable
    public override var isEditable: Bool {
        get {
            return false
        }
        set {
        }
    }
    
    /// TextView should not be selectable
    public override var isSelectable: Bool {
        get {
            return false
        }
        set {
        }
    }
    
    /// TextView should not allow scrolling
    public override var isScrollEnabled: Bool {
        get {
            return false
        }
        set {
        }
    }
    
    // MARK: - API
    // MARK: Adding actions
    
    /// Stores the passed action that needs to be executed when the
    /// specified occurence of the passed substring receives a tap.
    ///
    /// - Parameters:
    ///   - action: action to execute
    ///   - substring: substing to trigger the action
    ///   - occurence: a special ocurrence of the string (staring with 0).
    ///                If not set applies to all ocurrences
    ///   - attributes: text attributes to apply to the tapable areas
    public func addAction(
        action: (() -> Void)?,
        forSubstring substring: String,
        occurence: Int? = nil,
        attributes: [String: Any]? = nil) {
        
        // ignore call if substring is empty
        guard !substring.isEmpty else {
            return
        }
        
        // get ranges
        guard let ranges = self.rangesForOcurrences(
            ofSubstring: substring) else { return }
        
        // if occurence specified apply only to that
        if let index = occurence {
            // guard bounds
            guard index >= 0, index < ranges.count else { return }
            
            // add action
            let textViewAction = MTInteractiveTextViewAction.init()
            textViewAction.action = action
            textViewAction.actionRange = ranges[index]
            textViewAction.attributes = attributes
            self.apply(attributes: attributes, forRange: ranges[index])
            self.actions.append(textViewAction)
            return
        }
        
        // create actions
        for range in ranges {
            let textViewAction = MTInteractiveTextViewAction.init()
            textViewAction.action = action
            textViewAction.actionRange = range
            textViewAction.attributes = attributes
            self.apply(attributes: attributes, forRange: range)
            self.actions.append(textViewAction)
        }
    }
    
    /// Stores the passed action that needs to be executed when the
    /// specified range receives a tap.
    ///
    /// - Parameters:
    ///   - action: action to execute
    ///   - range: range to trigger the action
    ///   - attributes: text attributes to apply to the tapable areas
    public func addAction(
        action: (() -> Void)?,
        forRange range: NSRange,
        attributes: [String: Any]? = nil) {
        // guard range
        guard range.location >= 0, (range.location + range.length)
            < self.attributedText.string.count else {
            return
        }
        
        // add action
        let textViewAction = MTInteractiveTextViewAction.init()
        textViewAction.action = action
        textViewAction.actionRange = range
        textViewAction.attributes = attributes
        self.apply(attributes: attributes, forRange: range)
        self.actions.append(textViewAction)
    }
    
    /// Removes all added actions from the text view
    public func removeAllActions() {
        self.actions.removeAll()
    }
    
    // MARK: Debugging
    
    /// Highlights the bounding boxes of the tapable areas.
    /// Is ignored in production builds.
    public func highlightActions() {
        #if DEBUG
        for action in self.actions {
            let view = UIView.init(frame: self.boundingBoxForCharacters(
                inRange: action.actionRange ?? NSMakeRange(0, 0)))
            view.backgroundColor = UIColor.red.withAlphaComponent(0.5)
            self.addSubview(view)
        }
        #endif
    }
    
    // MARK: - Private
    
    /// Applies passed attributes to passed range
    ///
    /// - Parameters:
    ///   - attributes: text attributes
    ///   - forRange: range
    private func apply(attributes: [String: Any]?, forRange: NSRange) {
        guard let attr = attributes else { return }
        let text = NSMutableAttributedString.init(
            attributedString: self.attributedText)
        text.addAttributes(attr, range: forRange)
        self.attributedText = text
    }
    
    /// Returns the bounding box for the passed range argument
    ///
    /// - Parameter range: the text range to calculate the bounding box for
    /// - Returns: Bounding box of the text in the passed range
    private func boundingBoxForCharacters(inRange range: NSRange) -> CGRect {
        // get glyph range
        let glyphRange = self.layoutManager.glyphRange(
            forCharacterRange: range,
            actualCharacterRange: nil)
        
        // get text container
        if let textContainer = self.layoutManager.textContainer(
            forGlyphAt: glyphRange.location,
            effectiveRange: nil) {
            
            // get bounding box
            var boundingBox = self.layoutManager.boundingRect(
                forGlyphRange: glyphRange,
                in: textContainer)
            
            // take inset into consideration
            boundingBox.origin.x += self.textContainerInset.left
            boundingBox.origin.y += self.textContainerInset.top
            
            return boundingBox
        }
        
        return CGRect.init()
    }
    
    /// Returns the ranges for all ocurrences of the passed substring
    ///
    /// - Parameter ofSubstring: substring to look for
    /// - Returns: list of ranges containing the passed substring
    private func rangesForOcurrences(ofSubstring: String) -> [NSRange]? {
        let string = self.attributedText.string as NSString
        
        var ranges: [NSRange] = []
        var range: NSRange
        var remainingRange = NSMakeRange(0, string.length)

        repeat {
            // get next range
            range = string.range(
                of: ofSubstring,
                options: [],
                range: remainingRange)
            
            // add to list if valid
            if range.location != NSNotFound {
                ranges.append(range)
            } else {
                break
            }
            
            // calculate remaining range
            remainingRange = NSMakeRange(
                range.location + range.length,
                string.length - (range.location + range.length))
        } while range.location != NSNotFound
        
        return ranges.count == 0 ? nil : ranges
    }
  
}
