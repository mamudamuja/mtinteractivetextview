//
//  ViewController.swift
//  MTInteractiveTextView
//
//  Created by tomicmilutin on 11/27/2017.
//  Copyright (c) 2017 tomicmilutin. All rights reserved.
//

import UIKit

import MTInteractiveTextView

class ViewController: UIViewController {

    @IBOutlet weak var interactiveTextView: MTInteractiveTextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.interactiveTextView.addAction(action: {
            print("WORKS")
        }, forRange: NSRange.init(location: 2, length: 5),
           attributes:
            [NSUnderlineStyleAttributeName: NSUnderlineStyle.styleSingle.rawValue])
        
        self.interactiveTextView.addAction(action: {
            print("WORKS")
        }, forRange: NSRange.init(location: -1, length: 10))
        
        self.interactiveTextView.addAction(action: {
            print("WORKS")
        }, forRange: NSRange.init(location: 10, length: 200000))
        
        self.interactiveTextView.addAction(action: {
            print("WORKS")
        }, forSubstring: "dolor", occurence: -10, attributes: nil)
        
        self.interactiveTextView.addAction(action: {
            print("WORKS")
        }, forSubstring: "dolor", occurence: 0, attributes: nil)
        
        self.interactiveTextView.addAction(action: {
            print("WORKS")
        }, forSubstring: "dolor", occurence: 10, attributes: nil)
        
        self.interactiveTextView.addAction(action: {
            print("WORKS")
        }, forSubstring: "dolor",
           occurence: 1,
           attributes:
            [NSUnderlineStyleAttributeName: NSUnderlineStyle.styleSingle.rawValue])
        
        self.interactiveTextView.addAction(action: {
            print("WORKS")
        }, forSubstring: "ad")
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0, execute: {
            self.interactiveTextView.highlightActions()
        })
    }
}
