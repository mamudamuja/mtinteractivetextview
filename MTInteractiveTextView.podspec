Pod::Spec.new do |s|
  s.name             = 'MTInteractiveTextView'
  s.version          = '0.1.2'
  s.summary          = 'A UITextView subclass that makes it possible to add actions to text ranges.'
  s.description      = <<-DESC
This UITextView subclass makes it very easy to assing actions to a text ranges within the textview.
It also provides a quick way to highlight interactive parts of the string.
                       DESC

  s.homepage         = 'https://bitbucket.org/mamudamuja/mtinteractivetextview'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'tomicmilutin' => 'milutin@appbuilder.at' }
  s.source           = { :git => 'https://bitbucket.org/mamudamuja/mtinteractivetextview', :tag => s.version.to_s }

  s.ios.deployment_target = '9.0'
  s.source_files = 'MTInteractiveTextView/Classes/**/*'
  s.frameworks = 'UIKit'
end
