# MTInteractiveTextView

[![CI Status](http://img.shields.io/travis/tomicmilutin/MTInteractiveTextView.svg?style=flat)](https://travis-ci.org/tomicmilutin/MTInteractiveTextView)
[![Version](https://img.shields.io/cocoapods/v/MTInteractiveTextView.svg?style=flat)](http://cocoapods.org/pods/MTInteractiveTextView)
[![License](https://img.shields.io/cocoapods/l/MTInteractiveTextView.svg?style=flat)](http://cocoapods.org/pods/MTInteractiveTextView)
[![Platform](https://img.shields.io/cocoapods/p/MTInteractiveTextView.svg?style=flat)](http://cocoapods.org/pods/MTInteractiveTextView)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.


## Requirements

+ iOS 9+
+ Swift 3

## Usage

```swift
// 1. setup text view in Storyboard or code

// 2. add actions
self.interactiveTextView.addAction(action: {
	// code here
}, forSubstring: "dolor")
```

This code results in the following tappable actions.

![Example Image](example.jpeg)

## Installation

MTInteractiveTextView is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'MTInteractiveTextView'
```

## Author

tomicmilutin, milutin@appbuilder.at

## License

MTInteractiveTextView is available under the MIT license. See the LICENSE file for more info.
